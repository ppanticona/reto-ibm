package com.ppanticona.retoibm.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;

@Document(collection = "operacion")
public class Operacion implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private String id;


    @Field("sumandouno")
    private Integer sumandouno;


    @Field("sumandodos")
    private Integer sumandodos;


    @Field("resultado")
    private Integer resultado;


    public Operacion id(String id) {
        this.setId(id);
        return this;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getSumandouno() {
        return sumandouno;
    }

    public void setSumandouno(Integer sumandouno) {
        this.sumandouno = sumandouno;
    }

    public Integer getSumandodos() {
        return sumandodos;
    }

    public void setSumandodos(Integer sumandodos) {
        this.sumandodos = sumandodos;
    }

    public Integer getResultado() {
        return resultado;
    }

    public void setResultado(Integer resultado) {
        this.resultado = resultado;
    }
}
