package com.ppanticona.retoibm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RetoibmApplication {

	public static void main(String[] args) {
		SpringApplication.run(RetoibmApplication.class, args);
	}

}
