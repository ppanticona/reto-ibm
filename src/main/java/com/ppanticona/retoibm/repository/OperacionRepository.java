package com.ppanticona.retoibm.repository;

import com.ppanticona.retoibm.domain.Operacion;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OperacionRepository extends MongoRepository<Operacion, String> {
}
