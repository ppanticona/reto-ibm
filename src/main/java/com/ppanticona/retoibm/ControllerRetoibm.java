package com.ppanticona.retoibm;

import com.ppanticona.retoibm.domain.Operacion;
import com.ppanticona.retoibm.repository.OperacionRepository;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping
public class ControllerRetoibm {


    private final OperacionRepository operacionRepository;

    public ControllerRetoibm(OperacionRepository operacionRepository) {
        this.operacionRepository = operacionRepository;
    }


    @RequestMapping(value = "/sumar/{sum01}/{sum02}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    Map saludar(@PathVariable("sum01") Integer sum01, @PathVariable("sum02") Integer sum02) {
        Operacion operacionBean = new Operacion();
        operacionBean.setSumandouno(sum01);
        operacionBean.setSumandodos(sum02);

        operacionBean.setResultado(sum01+sum02);

        operacionRepository.save(operacionBean);
        return Collections.singletonMap("resultado", String.valueOf(sum01 + sum02));



    }


}
