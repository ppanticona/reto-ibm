package com.ppanticona.retoibm;


import com.ppanticona.retoibm.repository.OperacionRepository;
import org.junit.*;



public class ControllerRetoibmUTTest {


    private final OperacionRepository operacionRepository;

    public ControllerRetoibmUTTest(OperacionRepository operacionRepository){


        this.operacionRepository = operacionRepository;
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }


    @Test
    public void testSaludar() {
        System.out.println("saludar");
        Integer sum01 = 10;
        Integer sum02 = 20;

        ControllerRetoibm instance = new ControllerRetoibm(operacionRepository);
        Assert.assertNotNull(instance.saludar(sum01, sum02));
        Assert.assertEquals(32, Integer.parseInt(String.valueOf(instance.saludar(sum01, sum02).get("resultado"))));
    }

}
